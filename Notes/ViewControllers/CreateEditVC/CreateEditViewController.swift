//
//  CreateEditViewController.swift
//  Notes
//
//  Created by Ilya Rabyko on 8.08.21.
//

import UIKit

enum ContentType {
    case editList
    case unknown
}

class CreateEditViewController: UIViewController {

    
    @IBOutlet weak var mainTitle: UILabel!
    
    @IBOutlet weak var noteNameField: UITextView!
    @IBOutlet weak var descriptionNoteView: UITextView!
    @IBOutlet weak var downView: UIView!
    
    @IBOutlet var colorButtons: [UIButton]!
    @IBOutlet var imagesBut: [UIButton]!
    
    @IBOutlet weak var createButton: UIButton!

    var currentNote: Note = Note()
    
    var contentType: ContentType = .unknown
    
    var colorString = ""
    var noteTitle = ""
    var noteImage: NSData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if contentType == .editList {
            createButton.setTitle("Save", for: .normal)
            mainTitle.text = "Edit notes"
            noteNameField.text = noteTitle
            descriptionNoteView.text = currentNote.text
        for buttons in colorButtons {
            let currentColor = buttons.backgroundColor?.toRGBAString()
            if currentColor == currentNote.color {
                buttons.titleLabel?.text = "."
                let origImage = UIImage(named: "Selected")
                let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                buttons.setImage(tintedImage, for: .normal)
                buttons.tintColor = .white
            }
            buttons.titleLabel?.text = ""
        }
            for buttons in imagesBut {
                let dataImage = buttons.currentImage!.pngData() as NSData?
                buttons.titleLabel?.text = "."
                if  dataImage == currentNote.image {
                    buttons.layer.borderWidth = 2
                    buttons.layer.borderColor = UIColor.blue.cgColor
                }
                buttons.titleLabel?.text = ""
            }
        }

        noteNameField.delegate = self
        descriptionNoteView.delegate = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationSwipeBack()
        downView.roundCorners(corners: [.topLeft, .topRight], radius: 16)
        hideKeyboardWhenTappedAround()
        
    }
    
    func navigationSwipeBack() {
        let tapView = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction))
        tapView.direction = .right
        self.view.addGestureRecognizer(tapView)
    }
    
    @objc func swipeAction() {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    
    @IBAction func colorAction(_ sender: UIButton) {
        sender.titleLabel?.text = "."
        for buttons in colorButtons {
            if buttons.titleLabel?.text == "." {
                let origImage = UIImage(named: "Selected")
                let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                buttons.setImage(tintedImage, for: .normal)
                buttons.tintColor = .white
                colorString = (buttons.backgroundColor?.toRGBAString())!
                if contentType == .editList {
                    RealmManager.shared.color(note: currentNote, color: colorString)
                }
            } else if buttons.titleLabel?.text == "" {
                buttons.setImage(nil, for: .normal)
                
            }
            
        }
        sender.titleLabel?.text = ""
}
    
    @IBAction func imageAction(_ sender: UIButton) {
        sender.titleLabel?.text = "."
        for buttons in imagesBut {
            if buttons.titleLabel?.text == "." {
                buttons.layer.borderWidth = 2
                buttons.layer.borderColor = UIColor.blue.cgColor
                let dataImage = buttons.currentImage!.pngData() as NSData?
                noteImage = dataImage
                if contentType == .editList {
                    RealmManager.shared.image(note: currentNote, image: dataImage!)
                }
            } else if buttons.titleLabel?.text == "" {
                buttons.layer.borderWidth = 0
            }
            
        }
        sender.titleLabel?.text = ""
    }
    
    @IBAction func addListAction(_ sender: Any) {
        if contentType == .editList {
            guard let title = noteNameField.text else { return }
            RealmManager.shared.title(note: currentNote, title: title)
            guard let description = descriptionNoteView.text else { return }
            RealmManager.shared.text(note: currentNote, text: description)
            navigationController?.popViewController(animated: true)
        } else {
            
            let newNote: Note = Note()
            
            guard let title = noteNameField.text else { return }
            newNote.title = title
            guard let description = descriptionNoteView.text else { return }
            newNote.text = description
            guard let image = noteImage else { return }
            newNote.image = image
            newNote.color = colorString

        RealmManager.shared.writeObject(note: newNote)
        navigationController?.popViewController(animated: true)
    }
    }
    
    
    @IBAction func cancelAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
}

extension CreateEditViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let rangeOfTextToReplace = Range(range, in: textView.text) else {
            return false
        }
        let substringToReplace = textView.text[rangeOfTextToReplace]
        let count = textView.text.count - substringToReplace.count + text.count
        var charactersMax = 0
        switch textView.tag {
        case 1001:
            charactersMax = 100
        case 1002:
            charactersMax = 250
        default:
            print("Error")
        }
        return count <= charactersMax
    }
}
