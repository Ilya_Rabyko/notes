//
//  ViewingViewController.swift
//  Notes
//
//  Created by Ilya Rabyko on 8.08.21.
//

import UIKit

class ViewingViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var DescriptionLabel: UILabel!
    
    var titleNote = ""
    var descriptionNote = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleNote
        DescriptionLabel.text = descriptionNote
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        overrideUserInterfaceStyle = .light
        navigationSwipeBack()
    }
    
    func navigationSwipeBack() {
        let tapView = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction))
        tapView.direction = .right
        self.view.addGestureRecognizer(tapView)
    }
    
    @objc func swipeAction() {
        navigationController?.popViewController(animated: true)
    }
    
    



}
