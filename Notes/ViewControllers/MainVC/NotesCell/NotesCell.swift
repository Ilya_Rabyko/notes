//
//  NotesCell.swift
//  Notes
//
//  Created by Ilya Rabyko on 8.08.21.
//

import UIKit

class NotesCell: UITableViewCell {

    @IBOutlet weak var colorView: UIView!
    
    @IBOutlet weak var notesImage: UIImageView!
    @IBOutlet weak var titlelabel: UILabel!
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var blurEffect: UIVisualEffectView!
    
    @IBOutlet weak var hideConst: NSLayoutConstraint!
    @IBOutlet weak var blurHideConst: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
        cancelButton.isHidden = true
        editButton.isHidden = true
        deleteButton.isHidden = true
        blurEffect.isHidden = true
        
        navigationSwipeRight()
        navigationSwipeLeft()
        
    }
    
    func navigationSwipeLeft() {
        let tapView = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction))
        tapView.direction = .left
        self.addGestureRecognizer(tapView)
    }
    
    @objc func swipeAction() {
        blurEffect.isHidden = false
        editButton.isHidden = false
        deleteButton.isHidden = false
        cancelButton.isHidden = false
        UIView.animate(withDuration: 0.5) {
            self.cancelButton.layer.borderWidth = 2
            self.cancelButton.layer.borderColor = UIColor.gray.cgColor
            self.hideConst.constant = 10
            self.blurHideConst.constant = 0
            self.editButton.alpha = 1
            self.deleteButton.alpha = 1
            self.cancelButton.alpha = 1
            self.blurEffect.alpha = 0.8
        }
    }

    
    
    func navigationSwipeRight() {
        let tapView = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        tapView.direction = .right
        self.addGestureRecognizer(tapView)
    }
  
    @objc func swipeRight() {
        UIView.animate(withDuration: 0.5) {
            self.editButton.alpha = 0
            self.deleteButton.alpha = 0
            self.cancelButton.alpha = 0
            self.blurEffect.alpha = 0
        }
        self.blurHideConst.constant = -400
        self.hideConst.constant = -190
        self.cancelButton.layer.borderColor = UIColor.clear.cgColor
    }
    
    
    @IBAction func cancelAction(_ sender: Any) {
        swipeRight()
    }
    
    @IBAction func deleteAction(_ sender: UIButton) {
        swipeRight()
    }
    
    @IBAction func editAction(_ sender: Any) {
        swipeRight()
    }
    
    func setup(note: Note) {
        let origImage = UIImage(data: note.image! as Data)
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        notesImage.tintColor = .white
        notesImage.image = tintedImage
        let color = UIColor(hexString: String(note.color.dropLast(2)))
        colorView.backgroundColor = color
        titlelabel.text = note.title
    }


}
