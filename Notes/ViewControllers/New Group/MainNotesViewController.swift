//
//  ViewController.swift
//  Notes
//
//  Created by Ilya Rabyko on 8.08.21.
//

import UIKit

class MainNotesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var downView: UIView!
    
    var notes: [Note] = []
    @IBOutlet weak var topConstrTableView: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(NotesCell.self)
        tableView.setupDelegateData(self)
        tableView.reloadData()
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        downView.roundCorners(corners: [.topLeft, .topRight], radius: 16)
        notes = RealmManager.shared.readObjects()
        updateTable()
        
    }
    
    func updateTable() {
        if notes.count == 0 {
            tableView.isHidden = true
        } else {
            tableView.isHidden = false
        }
        tableReloadData()
    }
    
    func tableReloadData() {
        UIView.transition(with: tableView, duration: 0.2, options: .transitionCrossDissolve, animations: { () -> Void in self.tableView.reloadData() }, completion: nil);
    }

    @IBAction func createAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let vc = storyboard.instantiateViewController(withIdentifier: "CreateEditViewController") as? CreateEditViewController else { return }
                navigationController?.pushViewController(vc, animated: true)

    }
    
    @objc func deleteNoteAction(sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: tableView)
        guard let indexPath = tableView.indexPathForRow(at: point) else { return }
        RealmManager.shared.deleteOne(note: notes.remove(at: indexPath.row))
        updateTable()
    }
    
    @objc func editAction(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "CreateEditViewController") as? CreateEditViewController else { return }
        vc.contentType = .editList
        vc.currentNote = notes[sender.tag]
        vc.noteTitle = notes[sender.tag].title
        navigationController?.pushViewController(vc, animated: true)
    }
    
}




extension MainNotesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let vc = storyboard.instantiateViewController(withIdentifier: "ViewingViewController") as? ViewingViewController else { return }
        vc.descriptionNote = notes[indexPath.row].text
        vc.titleNote = notes[indexPath.row].title
                navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NotesCell.self), for: indexPath)
        guard let noteCell = cell as? NotesCell else {return cell}
        noteCell.setup(note: notes[indexPath.row])
        noteCell.editButton.tag = indexPath.row
        noteCell.deleteButton.addTarget(self, action: #selector(deleteNoteAction(sender:)), for: .touchUpInside)
        noteCell.editButton.addTarget(self, action: #selector(editAction(sender:)), for: .touchUpInside)
        return noteCell
    }
    
    
}

