//
//  RealmManager.swift
//  Notes
//
//  Created by Ilya Rabyko on 8.08.21.
//

import RealmSwift
import Foundation
class  RealmManager {
    
    
    static let shared = RealmManager()
    let realm = try! Realm()
    private init() {}
    
    
    func writeObject(note: Note) {
        try! realm.write {
            realm.add(note)
        }
    }
    
    func color(note: Note, color: String) {
        do {
        try! realm.write {
            note.color = color
        }
    }
    }
    
    func title(note: Note, title: String) {
        do {
        try! realm.write {
            note.title = title
        }
        }
    }
    
    func text(note: Note, text: String) {
        do {
        try! realm.write {
            note.text = text
        }
        }
    }
    
    func  image(note: Note, image: NSData) {
        do {
        try! realm.write {
            note.image = image
        }
    }

    }

    
    
    func readObjects() -> [Note] {
        return Array(realm.objects(Note.self))
    }
    
    func deleteOne(note: Note) {
        try! realm.write {
            realm.delete(note)
        }
    }
    
    func writeObjectArray(note: [Note]) {
        try! realm.write {
            realm.add(note)
        }
    }

    
    func deleteAll() {
        try! realm.write{
            realm.deleteAll()

        }
    }
}

