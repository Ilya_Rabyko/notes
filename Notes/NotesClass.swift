//
//  NotesClass.swift
//  Notes
//
//  Created by Ilya Rabyko on 8.08.21.
//

import Foundation
import Realm
import RealmSwift

class Note: Object {
    @objc dynamic var text = ""
    @objc dynamic var title = ""
    @objc dynamic var image: NSData?
    @objc dynamic var color = ""

    convenience init(title: String, text: String, image: NSData, color: String ) {
        self.init()
        self.title = title
        self.title = text
        self.image = image
        self.color = color
    }
}
